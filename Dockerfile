FROM ubuntu:18.04
WORKDIR /home/test

COPY requirements.txt requirements.txt

RUN apt-get update
RUN apt-get install -y python3
RUN apt-get install -y python3-pip
RUN pip3 install -r requirements.txt

COPY test.py test.py
COPY rec.py rec.py
COPY boot boot

EXPOSE 4369 5672 15672 25672 35197

ENTRYPOINT ["./boot"]