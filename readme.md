 Performing test tasks for Science Data Software
====================

The repository contains the code which performs the [test job](https://docs.google.com/document/d/1KRgG_bypJ91OEf6bXQVme_kKS-DeWIeRrzrR8lVnIMw/edit) 
The solution consists of two application client and machine.

A client application designed for input, send them the machine for calculations and store calculation result. Data transmission is done via RabbitMQ.

A client application designed for input, send them the machine for calculations and store calculation result. Data transmission is done via RabbitMQ.

 Requirement
-----------------------------------

To work you need the following software:
* docker
* docker-composer
* python 3
As well as stable connection to the Internet at the time of deployment containers

 Install and run
-----------------------------------

Machine and RabbitMQ are placed in a different container, the client is a terminal program.

To install and run containers with RabbitMQ and the machine, go to the project directory and execute the command from the command line "docker-compose up --build". After the corresponding message of successful launch, the containers will be ready for operation. To access RabbitMQ, go to [0.0.0.0:15672](http://0.0.0.0:15672/)

To run the client application, go to the directory with the project and enter the command "python sender.py" or "python3 sender.py" in the terminal, depending on the settings of the environment.

 Data exchange and protocols
-----------------------------------

Data exchange between the client program and the machine is done in json format.

The machine expects from the client, and the client sends a list that consists of 4 digits. Each digit is one of the parameters of the iris flower - sepal length in cm, sepal width in cm, petal length in cm, petal width in cm, respectively.

The machine sends a dictionary to the client. In the dictionary, the type of the message is transmitted using the key 'type', data is passed to the 'data' key, the session state is passed to the 'result' key.

If from the machine on the key 'result' came True - does this mean that the communication session is completed, for podoliany the work necessary to re-send the data packet to calculate.

If from the machine on the key 'result' came False - it means that session is not finished, it was just a message about the current process. See below the types of messages from the machine

Messages from the machine can be of three type:
    msg - message that you just need to display for the user. The message is kept by the key 'data'
    error - error message when processing data. The message is kept by the key 'data'.
    data - the result of the calculation is transmitted from the machine. The result is transmitted using the 'data' key. The result is a pdf file. To correctly display the file, the data on the 'data' key must be decoded from the base64 format and written to a file, for example, file.write (base64.b64decode (data ['data'])), where the file is opened by the open command with the 'rb'.

IMPORTANT! RabbitMQ further encodes all the lines in a binary string. To work correctly with data coming from RabbitMQ, they must be decoded.

 Action and error messages
-----------------------------------

The client displays the report of sending data to the server and in case of successful saving the result of the calculation on the disk.
The machine sends error messages that occur during the calculation and report generation process, and also sends a message to the client if the input data has been successfully received.

IMPORTANT! If the client sent the data, but there is no message that the machine has accepted them - this is a malfunction with the machine or RabbitMQ. See Installing and Running or documentation on docker and RabbitMQ.