#coding: utf-8
import json
import sys
import base64

from reportlab.pdfgen import canvas  
from reportlab.lib.units import cm 

from test import Transmitter
from test import Machine

with Transmitter(
        host = 'rabbit', 
        sendQueue='fromMachine', 
        receiverQueue = 'toMachine'
        ) as transmitter:

    def callback(ch, method, properties, body):
        outputData = None
        #Sending the message that the data was successfully received
        transmitter.publishMessage(
            json.dumps({
                'type':'msg', 
                'data':"Data is accepted and processed", 
                'result':False})
            )
        #Verification of input data on whether they are correct or not
        try:
            data = json.loads(body)
        except Exception:
            outputData = {
                'type':'error', 
                'data':'Error in the package. %s' % sys.exc_info()[1],
                'result':False
                }
        else:
            result = machine.predict([data])
            if result['error']:
                outputData = {
                    'type':'error', 
                    'data':'Machine error. %s' % result['msg'],
                    'result':True
                    }
            else:
                try:
                    doc = canvas.Canvas('report.pdf')
                    doc.drawString(
                        150,
                        800,
                        "The result of the calculation of\
                        the probability of the species iris"
                        )
                    doc.drawString(50, 760, "Input data :")
                    doc.drawString(100, 740, "sepal length (cm) - %s" % data[0])
                    doc.drawString(100, 720, "sepal width (cm) - %s" % data[1])
                    doc.drawString(100, 700, "petal length (cm) - %s" % data[2])
                    doc.drawString(100, 680, "petal width (cm) - %s" % data[3])
                    doc.drawString(
                        50, 
                        640,
                        "The probability of varieties of iris following is:"
                        )
                    doc.drawString(100, 620,
                                "iris %s - %s" % (result['data'][1][0], result['data'][0][0]))
                    doc.drawString(100, 600,
                                "iris %s - %s" % (result['data'][1][1], result['data'][0][1]))
                    doc.drawString(100, 580,
                                "iris %s - %s" % (result['data'][1][2], result['data'][0][2]))
                    doc.save()
                except Exception:
                    outputData = {
                        'type':'error', 
                        'data':'Error creating report. %s' % sys.exc_info()[1], 
                        'result':True
                        }
                else:
                    try:
                        outputFile = open('report.pdf', 'rb')
                        #The data from the file is encrypted so that there is no error when converting to json
                        outputData = {'type':'data', 'data': base64.b64encode(outputFile.read()).decode(), 'result':True}
                        outputFile.close()
                    except Exception:
                        outputData = {
                            'type':'error', 
                            'data':'An error occurred while forming the parcel. %s' % sys.exc_info()[1],
                            'result':True
                            }
        finally:
            transmitter.publishMessage(json.dumps(outputData))

    machine = Machine()
    transmitter.openChannel()
    print('Channel is open. Wait messages.')
    transmitter.startReceivingMessage(callback = callback)