from test import Transmitter
import sys
import json
import datetime
import base64

with Transmitter(
        host = '127.0.0.1', 
        sendQueue='toMachine', 
        receiverQueue = 'fromMachine'
        ) as transmitter:

    def callback(ch, method, properties, body):
        #The input data from the server must be additionally 
        #decorated - it encodes the chi in addition
        data = json.loads(body.decode())
        #We check the message type and perform the corresponding actions.
        #Types:
        #   msg - the message you want to display has arrived
        #   error - error to which is added the corresponding prefix
        #   data - pdf file with calculation results
        if data['type'] == 'msg':
            print(data['data'])
        elif data['type'] == 'error':
            print('Error on remote server. %s' % data['data'])
        else:
            timestamp = datetime.datetime.strftime(
                datetime.datetime.now(), 
                "%Y_%m_%d_%H_%M_%S"
                )
            fileName = 'report_%s.pdf'%timestamp
            file = open(fileName, 'wb')
            file.write(base64.b64decode(data['data']))
            file.close()
            print('Result saved is file %s' % fileName)
        #If the result is transmitted via the key in the resulting dictionary,
        #True, more data from the remote server will not be received during 
        #this exchange session. You need to go to the input of new data for 
        #sending.
        if data['result']:
            main()

    def main():
        nameList = [
            'sepal length (cm)', 
            'sepal width (cm)', 
            'petal length (cm)', 
            'petal width (cm)'
            ]
        data = []
        #Output in the loop requests for input of input parameters. 
        #A number is expected
        #If you enter the wrong parameter, a message is displayed and
        #the parameter is asked again
        for name in nameList:            
            while True:
                inData = input('Please input %r : ' % name)
                if inData.isdigit():
                    data.append(inData)
                    break
                elif (inData == 'exit'):
                    sys.exit(0)
                else:
                    print('Wrong format. Try egen.')
        transmitter.publishMessage(json.dumps(data))
        print('Data sent')

    transmitter.openChannel()
    print('For exit input: exit')
    main()
    transmitter.startReceivingMessage(callback = callback)