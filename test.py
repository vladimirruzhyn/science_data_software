#coding: utf-8
"""
Created on 28.09.18
@author Vladimir Ruzhynskay
@package test

Module for performing a test task from Science Data Software

The module consists of two classes - the Machine and the Transmitter. Class Machine
represents the Machine that calculates the iris species. Class Transmitter
responsible for receiving/sending data through the rabbitMQ server
"""

import os
import sys
import time
import socket
import pickle

import pika

from sklearn.svm import SVC
from sklearn import datasets


class Machine:
    """The Machine class represents the interface for working with the machine """

    def __init__(self):        
        self.__machine = None

    def __initMachine(self):
        """The method initializes and trains the machine"""

        clf = SVC(probability=True)
        iris = datasets.load_iris()
        x, y = iris.data, iris.target 
        self.__irisTargetNames  =   iris.target_names
        clf.fit(x, y)  
        self.__machine = pickle.dumps(clf) 
    
    def predict(self, data):
        """The method produces a forecast of iris species based on the input data

        Args:
            data: An array of input data on the basis of which the calculation
                takes place. The array must consist of data of type int or 
                float, the length of the array 4

        Returns:
                The method returns a dictionary. In which, by mistake, the 
            error is returned successfully or there is no calculation. Value
            passed on the key True or False. 
                If the error is transmitted by key, then the error message 
            is contained in the msg key. 
                If the error is passed to the key by mistake, then the data 
            result is passed to the data key by the result of the calculation,
            which consists of two arrays: the zero array is the probability 
            array of the iris type, the second is the name of the iris type. 
            The keys in the arrays correspond to each other.
        """
        result = None
        try:
            if self.__machine is None:
                self.__initMachine()
            clf = pickle.loads(self.__machine)
            probability = clf.predict_proba(data)
        except Exception:
            e = sys.exc_info()[1]
            result = {'error':True, 'msg': e.args[0]}
        else:
            result = {
                'error':False, 
                'data':[probability[0], self.__irisTargetNames]
                }
        return result

class  Transmitter:
    """The class is responsible for sending and receiving information via rabbitMQ"""

    def __init__(self, host, sendQueue, receiverQueue):
        """Inits Transmitter        

        Args:
            host: host on which rabbitMQ works.
            sendChannel: the name of the channel on which the data will be sent.
            receiverQueue: the name of the channel by which data will be received
        """
        self.connection = None
        self.sendChannel = None
        self.receiverChannel = None
        self.host = host
        self.sendQueue = sendQueue
        self.receiverQueue = receiverQueue

    def __enter__(self):
        return self

    def openChannel(self):
        """The method connects to the server and creates channels for sending
        and receiving data
        """

        self.__checkHost()
        self.connection = pika.BlockingConnection(
                pika.ConnectionParameters(host = self.host)
                )
        self.sendChannel = self.connection.channel()
        self.sendChannel.queue_declare(queue = self.sendQueue)
        self.receiverChannel = self.connection.channel()
        self.receiverChannel.queue_declare(queue = self.receiverQueue)

    def publishMessage(self, message):
        """The method sends a message on the send channel
        
        Args:
            message:sent message
        """

        self.sendChannel.basic_publish(
            exchange = '', 
            routing_key = self.sendQueue, 
            body = message
            )

    def startReceivingMessage(self, callback):
        """The method tunes the channel to receive information
        
        Args:
            callback:The function that is called when the data is received 
        from the channel
        """
        self.receiverChannel.basic_consume(
            callback, 
            queue = self.receiverQueue, 
            no_ack=True
            )
        self.receiverChannel.start_consuming()

    def dropConnection(self):
        """The method closes the connection to the remote server"""
        if self.receiverChannel is not None:
            self.receiverChannel.close()
        if self.sendChannel is not None:
            self.sendChannel.close()
        if self.connection is not None:
            self.connection.close()

    def __checkHost(self):
        """The method checks the availability of the remote server
        
        Raises:
            Exception: The method throws an exception if the remote server does not communicate for one minute.
        """
        count = 0
        while True:
            try:
                host = socket.gethostbyname(self.host)
                s = socket.create_connection((host, 15672), 2)
                break
            except:
                count += 1
            finally:
                if count == 6:
                    raise Exception(
                        'Can not connect to a \
                        rabbitMQ on host %s' % self.host
                        )
            time.sleep(10)
    
    def __exit__(self, type, value, traceback):
        self.dropConnection()
        return False